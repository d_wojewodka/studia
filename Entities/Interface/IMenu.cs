using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dominik {
    public interface IMenu {
        string getName();
        string getDescription();
        MenuType getType();
        double getPrice();
    }
}
