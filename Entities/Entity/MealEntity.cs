using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dominik {
    public class MealEntity : Menu, IMenu {
        private string name;
        private string description;
        private double price;
        private MenuType type;

        public MealEntity(string name, string description, double price, MenuType type) 
            : base(name, description, price, type) {
            this.name = name;
            this.description = description;
            this.price = price;
            this.type = type;
        }

        public string getDescription() { return description; }
        public string getName() { return name; }
        public double getPrice() { return price; }
        public MenuType getType() { return type; }
    }
}
