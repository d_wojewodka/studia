using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Dominik
{
    public class AccountEntity
    {
        private int id;
        private string username;
        private string email;
        private string password;
        private string name;
        private string lastName;

        public AccountEntity(string username, string email, string password, string name, string lastName) {
            this.username = username;
            this.email = email;
            this.password = EncryptPassword(password);
            this.name = name;
            this.lastName = lastName;
        }

        public AccountEntity(string username, string email, string name, string lastName) {
            this.username = username;
            this.email = email;
            this.password = "";
            this.name = name;
            this.lastName = lastName;
        }

        public static string EncryptPassword(string password) {
            using var sha256 = SHA256.Create();
            var secretBytes = Encoding.UTF8.GetBytes(password);
            var secretHash = sha256.ComputeHash(secretBytes);

            return Convert.ToHexString(secretHash);
        }

        public string getUsername() { return username; }
        public string getEmail() { return email; }
        public string getPassword() { return password; }
        public string getName() { return name; }
        public string getLastName() { return lastName; }

    }
}
