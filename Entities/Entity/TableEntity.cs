using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dominik {
    public enum TableStatus {
        AVAILABLE,
        BOOKED,
        BUSY,
    }

    public class TableEntity {
        private int id;
        private string name;
        private int size;
        private TableStatus status;
        private string credentials;

        public TableEntity(string name, int size, TableStatus status) {
            this.name = name;
            this.size = size;
            this.status = status;
            this.credentials = "";
        }

        public TableEntity(string name, int size, TableStatus status, string credentials) {
            this.name = name;
            this.size = size;
            this.status = status;
            this.credentials = credentials;
        }

        public string GetStatusName() {
            switch (status) {
                case TableStatus.AVAILABLE:
                    return "Wolny";
                case TableStatus.BOOKED:
                    return "Zarezerwowany";
                case TableStatus.BUSY:
                    return "Zaj�ty";

                default:
                    return "Nieznany";
            }
        }


        public string getName() { return name;  }
        public int getSize() { return size;  }
        public TableStatus getStatus() { return status;  }
        public string getCredentials() { return credentials; }

        public Color GetStatusColor() {
            switch (status) {
                case TableStatus.AVAILABLE:
                    return Color.Green;
                case TableStatus.BOOKED:
                    return Color.Red;
                case TableStatus.BUSY:
                    return Color.Orange;

                default:
                    return Color.Black;
            }
        }
    }
}
