using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Dominik {
    public enum MenuType {
        BREAKFAST,
        DINNER,
        SUPPER,
        DESSERT,
        APETIZER,
        HOT,
        COLD,
        ALCOHOL,
        DRINK,
        ZERO_ALCOHOL,
    }

    public class MenuEntity : IMenu {
        private int id;
        private string name;
        private string description;
        private MenuType type;
        private double price;


        public MenuEntity() { }

        public MenuEntity(IMenu menu) {
            this.name = menu.getName();
            this.description = menu.getDescription();
            this.type = menu.getType();
            this.price = menu.getPrice();
        }


        public string getDescription() { return description!; }
        public MenuType getType() { return type; }
        public string getName() { return name!; }
        public double getPrice() { return price; }

        public string getString() {
            string text = "";
            text += "\nName: " + name;
            text += "\n\tDescription: " + description;
            text += "\n\tPrice: " + price + "zł";
            text += "\n\tType: " + type.ToString();

            return text;
        }

        public string GetTypeName()
        {
            switch (this.type)
            {
                case MenuType.BREAKFAST:
                    return "Śniadanie";
                case MenuType.DINNER:
                    return "Obiad";
                case MenuType.SUPPER:
                    return "Kolacja";
                case MenuType.DESSERT:
                    return "Deser";
                case MenuType.APETIZER:
                    return "Przystawka";
                case MenuType.HOT:
                    return "Napoje gorące";
                case MenuType.COLD:
                    return "Napoje zimne";
                case MenuType.ALCOHOL:
                    return "Alkohole";
                case MenuType.DRINK:
                    return "Drinki";
                case MenuType.ZERO_ALCOHOL:
                    return "Alkohole 0%";

                default:
                    return "Nieznany";
            }
        }

        public static bool isDrink(MenuType type) {
            switch (type) {
                case MenuType.HOT:
                case MenuType.COLD:
                case MenuType.ALCOHOL:
                case MenuType.DRINK:
                case MenuType.ZERO_ALCOHOL:
                    return true;

                default:
                    return false;
            }
        }
    }


    public class Menu {
        private string name;
        private string description;
        private double price;
        private MenuType type;

        public Menu(string name, string description, double price, MenuType type) {
            this.name = name;
            this.description = description;
            this.price = price;
            this.type = type;
        }
    }
}

