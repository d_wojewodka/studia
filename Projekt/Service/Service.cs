using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dominik
{
    public enum AuthenticateStatus
    {
        Success,
        InvalidUser,
        WrongPassword,
        UserDisabled
    }
    public class Service
    {
        Database database;
        AccountEntity? account;

        public Service( Database database )
        {
            this.database = database;
        }

        public bool bookTable( string name, int size )
        {
            // check if selected name is not already booked
            return true;
        }

        public bool cancelBooking( string name )
        {
            // cancel if name is taken
            return true;
        }

        public string getActiveUser( )
        {
            if( account != null )
                return ( account.getLastName() + " " + account.getName() );
            return "AnonymousUser";
        }

        public AuthenticateStatus Authenticate( string username, string password )
        {
            AccountEntity? auth = database.FindAccount( username, AccountEntity.EncryptPassword(password) );

            if( auth != null )
            {
                account = auth;
                return AuthenticateStatus.Success;
            }

            return AuthenticateStatus.WrongPassword;

        }

        public void Logout( )
        {
            account = null;
        }

        public List<TableEntity> GetAvailableTables( )
        {
            return database.FindAvailableTables( );
        }

        public List<TableEntity> GetAllTables( )
        {
            return database.FindAllTables( );
        }

        public TableEntity GetTableByName( string table )
        {
            return database.FindTableByName( table );
        }

        internal List<OrderEntity> GetOrderByTableName(string table)
        {
            return database.FindOrdersByTableName( table );
        }

        public void BookTable(string table_name, string credentials)
        {
            database.UpdateTableStatus(table_name, credentials, TableStatus.BOOKED);
        }


        public double GetPrice(string menu_entry)
        {
            return database.GetEntryPrice(menu_entry);
        }

        public void RemoveFromOrder(string table_name, string menu_entry)
        {
            database.RemoveFromOrder(table_name, menu_entry);
        }

        internal void CancelBooking(string table_name)
        {
            database.UpdateTableStatus(table_name, "", TableStatus.AVAILABLE);
            database.RemoveAllOrders(table_name);
        }

        public List<MenuEntity> GetMenuByType(MenuType type)
        {
            return database.GetMenuByType(type);
        }

        internal void AddEntryToTable(string table_name, string menu_entry, int quantity)
        {
            database.UpdateOrderMenu(table_name, menu_entry, quantity);
        }
    }
}
