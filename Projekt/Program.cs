using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dominik {
    internal static class Program {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            DebugLog("/*", false);
            DebugLog("\tAby wyłączyć konsolę DEBUGOWANIA należy kliknąć prawym na nazwę", false);
            DebugLog("\tprojektu. Wybrać właściwości, następnie 'Typ danych wyjściowych'", false);
            DebugLog("\tzmienią na 'Aplikacja systemu Windows'.", false);
            DebugLog("*/", false);

            Database database = new Database("database.sqlite3");

            try {
                Service service = new Service(database);

                // To customize application configuration such as set high DPI settings or default font,
                // see https://aka.ms/applicationconfiguration.

                // .net >= 6.0
                ApplicationConfiguration.Initialize();

                //Application.SetHighDpiMode(HighDpiMode.SystemAware);
                //Application.EnableVisualStyles();
                //Application.SetCompatibleTextRenderingDefault(false);

                if (args.Length > 0) {
                   try {
                        if (service.Authenticate(args[0], args[1]) == AuthenticateStatus.Success)
                            Application.Run(new ManagerForm(service));
                        else
                            DebugLog("[Program.cs] args >> Niepoprawna nazwa użytkownika lub hasło");
                    } catch (Exception ex) {
                        DebugLog("[Program.cs] Pierwszym argumentem musi być nazwa użytkownika, a drugim hasło" + ex);
                    }
                }
                Application.Run(new LoginForm(service));
            } catch (Exception ex) {
                DebugLog("[Program.cs] Problem przy łączeniu z bazą danych -> " + ex);
            }
        }
		
        internal static void DebugLog(string message, bool time = true) {
            using (StreamWriter sw = File.AppendText(@"log.txt"))
            {
                sw.WriteLine(message);
            }

            if (!time)
                Console.WriteLine(message);
            else
                Console.WriteLine($"{DateTime.Now}.{DateTime.Now.Millisecond}\t{message}");
        }
    }
}
