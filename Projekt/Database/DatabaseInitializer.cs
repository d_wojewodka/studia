using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection.Metadata.Ecma335;

namespace Dominik {
    class DatabaseInitializer {
		SqliteConnection connection;
		
		public DatabaseInitializer(SqliteConnection connection) { 
			this.connection = connection;

            if (!CheckTableExists("accounts"))
                if (CreateAccountTable())
                    InsertAccount();

            if (!CheckTableExists("tables"))
				if (CreateTableTable())
					InsertTable();

            if (!CheckTableExists("menu_entries"))
                if (CreateMenuTable())
                    InsertMenu();


            if (!CheckTableExists("orders"))
                CreateOrderTable();
        }
		
        public bool CheckTableExists(string table) {
            var sql = $"SELECT * FROM sqlite_master WHERE type = 'table' AND name = '{table}'";
            if (connection.State == System.Data.ConnectionState.Open) {
                SqliteCommand command = new SqliteCommand(sql, connection);
                SqliteDataReader reader = command.ExecuteReader();
                if (reader.HasRows) {
                    Program.DebugLog($"[DataInitializer.cs] Tabela '{table}' istnieje");
                    return true;
                }
                return false;
            }
            else {
                throw new System.ArgumentException("Data.ConnectionState must be open");
            }
        }

		public bool CreateAccountTable() {
            Program.DebugLog("[DataInitializer.cs] Tworzenie tabeli 'accounts'...");

            try {
                String tableCommand =
				@"
				    CREATE TABLE IF NOT EXISTS accounts
                        (
                            id INTEGER PRIMARY KEY, 
                            username NVARCHAR(32) UNIQUE NOT NULL,
                            email NVARCHAR(32) UNIQUE NOT NULL,
                            password NVARCHAR(256) NOT NULL,
                            name NVARCHAR(32) NOT NULL,
                            last_name NVARCHAR(32) NOT NULL
                        )
                ";

                SqliteCommand createTable = new SqliteCommand(tableCommand, connection);
                createTable.ExecuteReader();
            } catch (Exception ex) {
                Program.DebugLog("[DataInitializer.cs] Błąd przy tworzeniu tabeli 'accounts' -> " + ex);
                return false;
			}

            return true;
		}
		
		public bool InsertAccount() {
            List<AccountEntity> accounts = new List<AccountEntity>() {
                new AccountEntity("DWojewódka", "DWojewódka@email.com", "haslo", "Dominik", "Wojewódka"),
                new AccountEntity("WWojtyczka", "WWojtyczka@email.com", "haslo", "Wojciech", "Wojtyczka"),
                new AccountEntity("WPolok", "WPolok@email.com", "haslo", "Wojciech", "Polok"),
                new AccountEntity("JMańkowski", "JMańkowski@email.com", "haslo", "Jacek", "Mańkowski"),
                new AccountEntity("admin", "admin@email.com", "admin", "admin", "admin")
            };

            try {
                Program.DebugLog("[DataInitializer.cs] Wypełnianie tabeli 'accounts' danymi...");
                SqliteCommand command = connection.CreateCommand();

                string values = "";
                foreach (AccountEntity account in accounts) {
                    values += $"('{account.getUsername()}', '{account.getEmail()}', '{account.getPassword()}', '{account.getName()}', '{account.getLastName()}'),";
                }

                command.CommandText = "INSERT INTO accounts (username, email, password, name, last_name) VALUES " + values.Remove(values.Length - 1) + ";";
                command.ExecuteReader();
            } catch (Exception ex) {
                Program.DebugLog("[DataInitializer.cs] Błąd przy wypełnianiu tabeli 'accounts' danymi -> " + ex);
                return false;
            }

            return true;
		}
		

		
		public bool CreateTableTable() {
            Program.DebugLog("[DataInitializer.cs] Tworzenie tabeli 'tables'...");

            try {
                String tableCommand =
                @"
				    CREATE TABLE IF NOT EXISTS tables
                        (
                            id INTEGER PRIMARY KEY, 
                            name NVARCHAR(32) UNIQUE NOT NULL,
                            size NVARCHAR(2) NOT NULL,
                            status NVARCHAR(32) NOT NULL,
                            credentials NVARCHAR(32)
                        )
                ";

                SqliteCommand createTable = new SqliteCommand(tableCommand, connection);
                createTable.ExecuteReader();
            } catch (Exception ex) {
                Program.DebugLog("[DataInitializer.cs] Błąd przy tworzeniu tabeli 'tables' -> " + ex);
                return false;
			}

            return true;
		}
		
		public bool InsertTable() {
            List<TableEntity> tables = new List<TableEntity>() {
                new TableEntity("Stolik-2-1", 2, TableStatus.BUSY, "Bez rezerwacji"),
                new TableEntity("Stolik-2-2", 2, TableStatus.AVAILABLE),
                new TableEntity("Stolik-2-3", 2, TableStatus.AVAILABLE),
                new TableEntity("Stolik-2-4", 2, TableStatus.BOOKED, "Frąszczak Daniel"),
                new TableEntity("Stolik-2-5", 2, TableStatus.AVAILABLE),
                new TableEntity("Stolik-2-6", 2, TableStatus.AVAILABLE),
                new TableEntity("Stolik-4-1", 4, TableStatus.AVAILABLE),
                new TableEntity("Stolik-4-2", 4, TableStatus.BUSY, "Bez rezerwacji"),
                new TableEntity("Stolik-4-3", 4, TableStatus.AVAILABLE),
                new TableEntity("Stolik-4-4", 4, TableStatus.AVAILABLE),
                new TableEntity("Stolik-6-1", 6, TableStatus.BOOKED, "Gracjan Maryna"),
                new TableEntity("Stolik-6-2", 6, TableStatus.AVAILABLE),
                new TableEntity("Stolik-8-1", 8, TableStatus.AVAILABLE),
                new TableEntity("Stolik-8-2", 8, TableStatus.BOOKED, "Saddam Aller"),
            };

            try {
                Program.DebugLog("[DataInitializer.cs] Wypełnianie tabeli 'tables' danymi...");
                SqliteCommand command = connection.CreateCommand();

                string values = "";
                foreach (TableEntity table in tables) {
                    values += $"('{table.getName()}', '{table.getSize().ToString()}', '{table.getStatus().ToString()}', '{table.getCredentials().ToString()}'),";
                }

                command.CommandText = "INSERT INTO tables (name, size, status, credentials) VALUES " + values.Remove(values.Length - 1) + ";";
                command.ExecuteReader();
            } catch (Exception ex) {
                Program.DebugLog("[DataInitializer.cs] Błąd przy wypełnianiu tabeli 'tables' danymi -> " + ex);
                return false;
            }

            return true;
		}
		


		public bool CreateMenuTable() {
            Program.DebugLog("[DataInitializer.cs] Tworzenie tabeli 'menu_entries'...");

            try {
                String tableCommand =
                @"
				    CREATE TABLE IF NOT EXISTS menu_entries
                        (
                            id INTEGER PRIMARY KEY, 
                            name NVARCHAR(32) UNIQUE NOT NULL,
                            description NVARCHAR(128) NOT NULL,
                            type NVARCHAR(32) NOT NULL,
                            price REAL NOT NULL
                        )
                ";

                SqliteCommand createTable = new SqliteCommand(tableCommand, connection);
                createTable.ExecuteReader();
            }
            catch (Exception ex) {
                Program.DebugLog("[DataInitializer.cs] Błąd przy tworzeniu tabeli 'menu_entries' -> " + ex);
                return false;
            }

            return true;
        }

        public bool InsertMenu() {
            List<MenuEntity> menu_entries = new List<MenuEntity>() {
                //string name, string description, string allergens, double price, MealType type
                new MenuEntity(new MealEntity("Rolada z kapustą", "200g rolad, 300g klusek, 150g kapusty czerwonej", 21.99, MenuType.DINNER)),
                new MenuEntity(new MealEntity("Kebab", "200g wołowiny, sos, sałatki", 18.99, MenuType.DINNER)),
                new MenuEntity(new MealEntity("Kurczak w sosie słodko-kwaśnym", "150g kurczaka, 300g ryżu, 200g surówki coleslaw", 16.99, MenuType.DINNER)),
                new MenuEntity(new MealEntity("Sajgonki z mięsem", "3szt", 9.99, MenuType.APETIZER)),
                new MenuEntity(new MealEntity("Sajgonki z warzywami", "3szt", 7.99, MenuType.APETIZER)),
                new MenuEntity(new MealEntity("Krem brulee", "150g", 13.99, MenuType.DESSERT)),
                new MenuEntity(new MealEntity("Śniadanie angielskie", "Parówki, fasolka, jajko sadzone...", 17.99, MenuType.BREAKFAST)),
                new MenuEntity(new MealEntity("Omlet wieloziarnisty", "Z 4 jajek", 13.99, MenuType.BREAKFAST)),
                new MenuEntity(new MealEntity("Fasolka po bretońsku", "400g", 13.00, MenuType.SUPPER)),

                new MenuEntity(new DrinkEntity("Cola 330ml", "Puszka", 5.00, MenuType.COLD)),
                new MenuEntity(new DrinkEntity("Pepsi 330ml", "Puszka", 4.50, MenuType.COLD)),
                new MenuEntity(new DrinkEntity("Fanta 330ml", "Puszka", 5.00, MenuType.COLD)),
                new MenuEntity(new DrinkEntity("Mohito bezalkoholowe", "Wysoka szklanka", 8.00, MenuType.ZERO_ALCOHOL)),
                new MenuEntity(new DrinkEntity("Lech 0,4L", "Kufel", 7.00, MenuType.ALCOHOL)),
                new MenuEntity(new DrinkEntity("Lech 0,5L", "Kufel", 9.00, MenuType.ALCOHOL)),
                new MenuEntity(new DrinkEntity("Herbata", "Filiżanka", 8.00, MenuType.HOT)),
                new MenuEntity(new DrinkEntity("Kawa", "Kubek", 11.00, MenuType.HOT)),
                new MenuEntity(new DrinkEntity("Tequila Sunrise", "Wysoka szklanka", 17.00, MenuType.DRINK)),
                new MenuEntity(new DrinkEntity("Spritz Aperol", "Kieliszek", 15.00, MenuType.DRINK)),
                new MenuEntity(new DrinkEntity("Long Beach", "Wysoka szklanka", 21.00, MenuType.DRINK)),
            };

            try {
                Program.DebugLog("[DataInitializer.cs] Wypełnianie tabeli 'menu_entries' danymi...");
                SqliteCommand command = connection.CreateCommand();

                string values = "";
                foreach (MenuEntity menu in menu_entries) {
                    values += $"('{menu.getName()}', '{menu.getDescription()}', '{menu.getPrice() * 1.00}', '{menu.getType().ToString()}'),";
                }

                command.CommandText = "INSERT INTO menu_entries (name, description, price, type) VALUES " + values.Remove(values.Length - 1) + ";";
                command.ExecuteReader();
            }
            catch (Exception ex) {
                Program.DebugLog("[DataInitializer.cs] Błąd przy wypełnianiu tabeli 'menu_entries' danymi -> " + ex);
                return false;
            }

            return true;
        }

		public bool CreateOrderTable() {
            Program.DebugLog("[DataInitializer.cs] Tworzenie tabeli 'orders'...");

            try
            {
                String tableCommand =
                @"
				    CREATE TABLE IF NOT EXISTS orders
                        (
                            id INTEGER PRIMARY KEY, 
                            table_name NVARCHAR(32) NOT NULL,
                            menu_entry NVARCHAR(32) NOT NULL,
                            quantity INTEGER NOT NULL,
                            type NVARCHAR(32) NOT NULL
                        )
                ";

                SqliteCommand createTable = new SqliteCommand(tableCommand, connection);
                createTable.ExecuteReader();
            }
            catch (Exception ex)
            {
                Program.DebugLog("[DataInitializer.cs] Błąd przy tworzeniu tabeli 'orders' -> " + ex);
                return false;
            }





            List<OrderEntity> orders = new List<OrderEntity>() {
                new OrderEntity("Stolik-2-1", "Lech 0,4L", 4, MenuType.ALCOHOL),

                new OrderEntity("Stolik-2-4", "Cola 330ml", 1, MenuType.COLD),
                new OrderEntity("Stolik-2-4", "Lech 0,4L", 1, MenuType.ALCOHOL),
                new OrderEntity("Stolik-2-4", "Rolada z kapustą", 1, MenuType.DINNER),
                new OrderEntity("Stolik-2-4", "Sajgonki z mięsem", 2, MenuType.APETIZER),

                new OrderEntity("Stolik-6-1", "Cola 330ml", 2, MenuType.COLD),
                new OrderEntity("Stolik-6-1", "Pepsi 330ml", 1, MenuType.COLD),
                new OrderEntity("Stolik-6-1", "Fanta 330ml", 3, MenuType.COLD),
                new OrderEntity("Stolik-6-1", "Mohito bezalkoholowe", 4, MenuType.ZERO_ALCOHOL),
                new OrderEntity("Stolik-6-1", "Lech 0,4L", 1, MenuType.ALCOHOL),
                new OrderEntity("Stolik-6-1", "Lech 0,5L", 8, MenuType.ALCOHOL),
                new OrderEntity("Stolik-6-1", "Herbata", 3, MenuType.HOT),
                new OrderEntity("Stolik-6-1", "Kawa", 1, MenuType.HOT),

                new OrderEntity("Stolik-4-2", "Pepsi 330ml", 1, MenuType.COLD),
                new OrderEntity("Stolik-4-2", "Cola 330ml", 2, MenuType.COLD),
                new OrderEntity("Stolik-4-2", "Mohito bezalkoholowe", 3, MenuType.ZERO_ALCOHOL),
                new OrderEntity("Stolik-4-2", "Lech 0,4L", 1, MenuType.ALCOHOL),
                new OrderEntity("Stolik-4-2", "Lech 0,5L", 5, MenuType.ALCOHOL),
                new OrderEntity("Stolik-4-2", "Tequila Sunrise", 2, MenuType.DRINK),
                new OrderEntity("Stolik-4-2", "Spritz Aperol", 3, MenuType.DRINK),
                new OrderEntity("Stolik-4-2", "Long Beach", 5, MenuType.DRINK),

                new OrderEntity("Stolik-8-2", "Sajgonki z mięsem", 4, MenuType.APETIZER),
                new OrderEntity("Stolik-8-2", "Sajgonki z warzywami", 1, MenuType.APETIZER),
                new OrderEntity("Stolik-8-2", "Krem brulee", 2, MenuType.APETIZER),
                new OrderEntity("Stolik-8-2", "Kurczak w sosie słodko-kwaśnym", 3, MenuType.DINNER),
                new OrderEntity("Stolik-8-2", "Kebab", 2, MenuType.DINNER),
                new OrderEntity("Stolik-8-2", "Rolada z kapustą", 2, MenuType.DINNER),
                new OrderEntity("Stolik-8-2", "Kawa", 7, MenuType.HOT),
                new OrderEntity("Stolik-8-2", "Herbata", 1, MenuType.HOT),
                new OrderEntity("Stolik-8-2", "Śniadanie angielskie", 2, MenuType.BREAKFAST),
                new OrderEntity("Stolik-8-2", "Pepsi 330ml", 1, MenuType.COLD),
                new OrderEntity("Stolik-8-2", "Cola 330ml", 2, MenuType.COLD),
                new OrderEntity("Stolik-8-2", "Mohito bezalkoholowe", 2, MenuType.ZERO_ALCOHOL),
            };

            try
            {
                Program.DebugLog("[DataInitializer.cs] Wypełnianie tabeli 'orders' danymi...");
                SqliteCommand command = connection.CreateCommand();

                string values = "";
                foreach (OrderEntity order in orders)
                {
                    values += $"('{order.getTable()}', '{order.getMenu()}', '{order.getQuantity()}', '{order.getType().ToString()}'),";
                }

                command.CommandText = "INSERT INTO orders (table_name, menu_entry, quantity, type) VALUES " + values.Remove(values.Length - 1) + ";";
                command.ExecuteReader();
            }
            catch (Exception ex)
            {
                Program.DebugLog("[DataInitializer.cs] Błąd przy wypełnianiu tabeli 'orders' danymi -> " + ex);
                return false;
            }


            return true;
        }

    }
}