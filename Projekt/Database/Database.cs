using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Net;

namespace Dominik {
    public class Database {
		SqliteConnection connection;
		
		public Database(string filename) { 
			// Create a new database connection:
            Program.DebugLog("[Database.cs] Nawiązywanie połączenia z bazą danych...");
            connection = new SqliteConnection($"Filename={filename}");

            // Open the connection:
            try {
                connection.Open();
                Program.DebugLog("[Database.cs] Nawiązywano połączenia z bazą danych.");
            }
            catch (Exception ex) {
                Program.DebugLog("[Database.cs] Problem w nawiązywaniu połączenia z bazą danych -> " + ex);
			}
			
			// Initialize database tables / data
			new DatabaseInitializer(connection);
		}
		
		public AccountEntity FindAccount(string search, string password) {
			Program.DebugLog($"[Database.cs] Szukanie w tabeli 'accounts' danych: {search} " + new string('*', password.Length));
			SqliteCommand command = connection.CreateCommand();

			command.CommandText =
            @"
				SELECT * FROM accounts WHERE 
				(LOWER(username) = $search AND password = $password) OR 
				(LOWER(email) = $search AND password = $password)
			";

			command.Parameters.AddWithValue("$search", search.ToLower());
			command.Parameters.AddWithValue("$password", password);
			SqliteDataReader reader = command.ExecuteReader();	
			
			if (reader.HasRows)
			{
                Program.DebugLog("[Database.cs] Znaleziono dane w tabeli 'accounts'");

                while (reader.Read()) {
					return new AccountEntity(
						reader.GetString(1),	// username
						reader.GetString(2),	// email
						reader.GetString(4),	// name
						reader.GetString(5)		// lastName
					);
				}
			}

			return null;
		}


        public List<TableEntity> FindAvailableTables() {
			Program.DebugLog($"[Database.cs] Szukanie wolnych stolików w tabeli 'tables'");
			SqliteCommand command = connection.CreateCommand();
			List<TableEntity> tables = new List<TableEntity>();

			command.CommandText =
			@"
				SELECT * FROM tables WHERE
				available = $status
			";

			command.Parameters.AddWithValue("$status", TableStatus.AVAILABLE);

			SqliteDataReader reader = command.ExecuteReader();

			if (reader.HasRows)
			{
				Program.DebugLog("[Database.cs] Znaleziono dane w tabeli 'tables'");

				while (reader.Read())
				{
					Enum.TryParse(reader.GetString(3), out TableStatus status);

					tables.Add(new TableEntity(
						reader.GetString(1),   // name
						reader.GetInt32(2),    // size
						status   // available
					));
				}
			}
			else
				return null;

			return tables;
        }

        internal List<OrderEntity> FindOrdersByTableName(string table) {
			Program.DebugLog($"[Database.cs] Szukanie zamówień dla stolika " + table + " w tabeli 'tables'");
			SqliteCommand command = connection.CreateCommand();
			List<OrderEntity> orders = new List<OrderEntity>();

			command.CommandText =
				@"
					SELECT * FROM orders WHERE
					table_name = $table
				";

			command.Parameters.AddWithValue("$table", table);

			SqliteDataReader reader = command.ExecuteReader();

			if (reader.HasRows)
			{
				Program.DebugLog("[Database.cs] Znaleziono zamówienia dla stolika " + table + " w tabeli 'tables'");

				while (reader.Read())
				{
					Enum.TryParse(reader.GetString(4), out MenuType type);

                    orders.Add(new OrderEntity(
						reader.GetString(1),   // table_name
						reader.GetString(2),   // menu_name
						reader.GetInt32(3),    // size
						type
					));
				}
			}
			else
				return null;

			return orders;
        }

        public List<TableEntity> FindAllTables() {
            Program.DebugLog($"[Database.cs] Szukanie wolnych stolików w tabeli 'tables'");
            SqliteCommand command = connection.CreateCommand();
            List<TableEntity> tables = new List<TableEntity>();

            command.CommandText =
            @"
				SELECT * FROM tables
			";

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows) {
                Program.DebugLog("[Database.cs] Znaleziono dane w tabeli 'tables'");

                while (reader.Read()) {
					Enum.TryParse(reader.GetString(3), out TableStatus status);

                    tables.Add(new TableEntity(
                        reader.GetString(1),
                        reader.GetInt32(2),
                        status,
						reader.GetString(4)
                    ));
                }
            }
            else
                return tables;

            return tables;
        }
		
		public TableEntity FindTableByName(string table_name) {
			Program.DebugLog($"[Database.cs] Szukanie stolika '{table_name}' w tabeli 'tables'");
            SqliteCommand command = connection.CreateCommand();
            TableEntity table = null;

            command.CommandText =
            @"
				SELECT * FROM tables WHERE
				name = $table_name
			";

			command.Parameters.AddWithValue("$table_name", table_name);

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows) {
                Program.DebugLog($"[Database.cs] Znaleziono stolik '{table_name}' w tabeli 'tables'");

                while (reader.Read()) {
					Enum.TryParse(reader.GetString(3), out TableStatus status);
                    table = new TableEntity(reader.GetString(1), reader.GetInt32(2), status);
					
					return table;
                }

				return table;
            }
            else
                return table;
		}
		
		
		
		public TableEntity FindInTable(string table_name, List<string> qualifier, List<string> value) {
			Program.DebugLog($"[Database.cs] Szukanie stolika '{table_name}' w tabeli 'tables'");
            SqliteCommand command = connection.CreateCommand();
            TableEntity table = null;

            command.CommandText =
            @"
				SELECT * FROM tables WHERE
				name = $table_name
			";

			command.Parameters.AddWithValue("$table_name", table_name);

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows) {
                Program.DebugLog($"[Database.cs] Znaleziono stolik '{table}' w tabeli 'tables'");

                while (reader.Read()) {
					Enum.TryParse(reader.GetString(3), out TableStatus status);
                    table = new TableEntity(reader.GetString(1), reader.GetInt32(2), status);
					
					return table;
                }

				return table;
            }
            else
                return table;
		}

        public void UpdateTableStatus(string table_name, string credentials, TableStatus status)
        {
            Program.DebugLog($"[Database.cs] Szukanie stolika '{table_name}' w tabeli 'tables'");
            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @"
				UPDATE tables
				SET credentials = $credentials,  status = $status
				WHERE name = $table_name
			";

            command.Parameters.AddWithValue("$credentials", credentials);
            command.Parameters.AddWithValue("$status", status);
            command.Parameters.AddWithValue("$table_name", table_name);

            SqliteDataReader reader = command.ExecuteReader();
        }

		public double GetEntryPrice(string menu_entry)
		{
            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @$"
				SELECT price FROM menu_entries WHERE
				name = $menu_entry
			";

            command.Parameters.AddWithValue("$menu_entry", menu_entry);

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
					Double.TryParse(reader.GetString(0), out double price);

					if (price == 0)
						return reader.GetDouble(0);

                    return price;
                }

				return 0.0;
            }
            else
                return 0.0;
        }

        public string GetEntryType(string menu_entry)
        {
            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @$"
				SELECT type FROM menu_entries WHERE
				name = $menu_entry
			";

            command.Parameters.AddWithValue("$menu_entry", menu_entry);

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return reader.GetString(0);
                }

                return "";
            }
            else
                return "";
        }

        public void RemoveFromOrder(string table_name, string menu_entry)
        {
            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @"
				DELETE FROM orders 
				WHERE (table_name = $table_name AND menu_entry = $menu_entry)
			";

            command.Parameters.AddWithValue("$menu_entry", menu_entry);
            command.Parameters.AddWithValue("$table_name", table_name);

            SqliteDataReader reader = command.ExecuteReader();
        }

        public void RemoveAllOrders(string table_name)
        {
            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @"
				DELETE FROM orders 
				WHERE (table_name = $table_name)
			";

            command.Parameters.AddWithValue("$table_name", table_name);

            SqliteDataReader reader = command.ExecuteReader();
        }

        internal List<MenuEntity> GetMenuByType(MenuType type)
        {
			List<MenuEntity> menu_list = new List<MenuEntity>();

            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @$"
				SELECT * FROM menu_entries WHERE
				type = $type
			";

            command.Parameters.AddWithValue("type", type.ToString());

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
					MenuEntity entry = new MenuEntity();

					string name = reader.GetString(1);
					double price;

                    Double.TryParse(reader.GetString(4), out price);

                    if (price == 0)
                        price = reader.GetDouble(4);


					if (MenuEntity.isDrink(type))
						menu_list.Add(new MenuEntity(new DrinkEntity(name, reader.GetString(2), price, type)));
					else
                        menu_list.Add(new MenuEntity(new MealEntity(name, reader.GetString(2), price, type)));
                }
            }

			return menu_list;
        }

		internal int GetOrderFromTable(string table_name, string menu_entry)
        {

            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @"
				SELECT quantity FROM orders
				WHERE
				menu_entry = $menu_entry AND table_name = $table_name
			";


            command.Parameters.AddWithValue("$table_name", table_name);
            command.Parameters.AddWithValue("$menu_entry", menu_entry);

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows) {
                while (reader.Read()) {
                    return reader.GetInt16(0);
                }
            }

			return 0;

        }


        public bool IsTableEmpty(string table_name)
        {
            SqliteCommand command = connection.CreateCommand();

            command.CommandText =
            @"
				SELECT * FROM tables WHERE
				name = $table_name
			";

            command.Parameters.AddWithValue("$table_name", table_name);

            SqliteDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {

                while (reader.Read())
                {
                    if (reader.GetString(4) == "" && reader.GetString(4).Length == 0)
                        return true;
                }

                return false;
            }
            else
                return false;
        }

        internal void UpdateOrderMenu(string table_name, string menu_entry, int quantity) {
			int qty = GetOrderFromTable(table_name, menu_entry);

            SqliteCommand command = connection.CreateCommand();

            if (IsTableEmpty(table_name))
                UpdateTableStatus(table_name, "Bez rezerwacji", TableStatus.BUSY);


            if (qty != 0) {
                command.CommandText =
                @"
					UPDATE orders
						SET quantity = $quantity
					WHERE
						table_name = $table_name AND menu_entry = $menu_entry
				";

                command.Parameters.AddWithValue("$quantity", qty + quantity);
                command.Parameters.AddWithValue("$table_name", table_name);
                command.Parameters.AddWithValue("$menu_entry", menu_entry);
            }
			else {
                command.CommandText =
				@"
					INSERT INTO orders 
						(table_name, menu_entry, quantity, type) 
					VALUES 
						($table_name, $menu_entry, $quantity, $type)
				";

                command.Parameters.AddWithValue("$table_name", table_name);
                command.Parameters.AddWithValue("$menu_entry", menu_entry);
                command.Parameters.AddWithValue("$quantity", quantity);
                command.Parameters.AddWithValue("$type", GetEntryType(table_name));
            }

            command.ExecuteReader();
        }
    }
}