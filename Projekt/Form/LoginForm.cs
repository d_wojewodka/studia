using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dominik
{
    public partial class LoginForm : Form
    {
        Service service;

        private bool _isPasswordVisible;

        public bool IsPasswordVisible
        {
            get => _isPasswordVisible; set
            {
                _isPasswordVisible = value;

                if( value == true )
                {
                    passwordTextBox.UseSystemPasswordChar = false;
                    VisibilityPicture.Image = global::Projekt.Properties.Resources.visibility_off_dark;
                }
                else
                {
                    passwordTextBox.UseSystemPasswordChar = true;
                    VisibilityPicture.Image = global::Projekt.Properties.Resources.visibility_dark;
                }

            }
        }

        public LoginForm( Service service )
        {
            this.service = service;

            InitializeComponent( );
            Init( );
        }
        private void Init( )
        {
            IsPasswordVisible = false;
            StartPosition = FormStartPosition.CenterScreen;
        }


        private void usernameTextBox_TextChanged( object sender, KeyEventArgs e )
        {
            UsernameLabelMessage.Text = string.Empty;
            if( e.KeyCode == Keys.Enter )
            {
                passwordTextBox.Focus( );
            }
        }

        private void passwordTextBox_TextChanged( object sender, KeyEventArgs e )
        {
            PasswordLabelMessage.Text = string.Empty;
            if( e.KeyCode != Keys.Enter )
                return;

            authenticateButton_Click( sender, e );

        }
        private void authenticateButton_Click( object sender, EventArgs e )
        {
            string username = usernameTextBox.Text.Trim( );
            string password = passwordTextBox.Text.Trim( );
            AuthenticateStatus status;

            if( username == "" )
            {
                UsernameLabelMessage.Text = "Nazwa użytkownika nie może być pusta";
                return;
            }

            if( password == "" )
            {
                PasswordLabelMessage.Text = "Hasło nie może być puste";
                return;
            }

            status = service.Authenticate( username, password );

            if( status == AuthenticateStatus.Success )
            {
                ManagerForm managerForm = new ManagerForm( service );

                Hide( );
                managerForm.Show( );
            }
            else
            {
                UsernameLabelMessage.Text = "Niepoprawna nazwa użytownika lub hasło";
                return;
            }
        }


        protected override void OnFormClosing( FormClosingEventArgs e )
        {
            Application.Exit( );
        }

        private void ClearTextBoxAfterClick(object sender, EventArgs e)
        {
            if( sender.GetType( ) != typeof( TextBox ) )
                return;

            TextBox textBox = (TextBox)sender;

            textBox.Clear( );
            
        }
        private void VisibilityPicture_Click( object sender, EventArgs e )
        {
            IsPasswordVisible = !IsPasswordVisible;
        }
        private Point MousePoints = new Point( );
        private void PanelHeader_MouseDown( object sender, MouseEventArgs e )
        {
            MousePoints = new Point( -e.X, -e.Y );
        }

        private void PanelHeader_MouseMove( object sender, MouseEventArgs e )
        {
            if( e.Button != MouseButtons.Left )
                return;
            Point mousePose = MousePosition;
            mousePose.Offset( MousePoints.X, MousePoints.Y );
            Location = mousePose;
        }


        private void FormMinimize_Click( object sender, EventArgs e )
        {
            WindowState = FormWindowState.Minimized;
        }

        private void FormClose_Click( object sender, EventArgs e )
        {
            Application.Exit( );
        }

        private void FormClose_MouseEnter( object sender, EventArgs e )
        {
            FormClose.BackColor = Color.Firebrick;
        }

        private void FormClose_MouseLeave( object sender, EventArgs e )
        {
            FormClose.BackColor = Color.IndianRed;
        }

        private void FormMinimize_MouseEnter( object sender, EventArgs e )
        {
            FormMinimize.BackColor = Color.FromArgb( 11, 53, 178 );
        }

        private void FormMinimize_MouseLeave( object sender, EventArgs e )
        {
            FormMinimize.BackColor = Color.FromArgb( 31, 73, 198 );
        }

    }
}
