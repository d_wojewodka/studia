using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt;

namespace Dominik {
    public partial class SelectMenuForm : Form {
        private Service service;
        private string table_name;
        private ManagerForm form;

        private Button selected;

        public SelectMenuForm(Service service, string table_name, ManagerForm form) {
			this.service = service;
            this.table_name = table_name;
            this.form = form;
            InitializeComponent();
        }

        private void ManagerForm_Load(object sender, EventArgs e) {
            SelectMenuForm test = (SelectMenuForm)sender;
            SelectButton(test.button4);

            RefreshMenu(service.GetMenuByType(MenuType.BREAKFAST));
        }


        public void RefreshMenu(List<MenuEntity> menu_list)
        {
            menu.Items.Clear();

            try
            {
                foreach (MenuEntity entity in menu_list)
                {
                    ListViewItem item = new ListViewItem(entity.getName());
                    item.SubItems.Add(entity.getPrice().ToString("0.00") + "zł");

                    menu.Items.Add(item);
                }
            }
            catch (ArgumentOutOfRangeException) { }
        }

        public void SelectButton(Button button)
        {
            if (selected != null)
                selected.BackColor = Color.White;

            button.BackColor = Color.MediumAquamarine;
            selected = button;
        }


        private void breakfast_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.BREAKFAST));
        }

        private void dinner_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.DINNER));
        }

        private void apetizer_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.APETIZER));
        }

        private void supper_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.SUPPER));
        }

        private void dessert_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.DESSERT));
        }


        private void cold_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.COLD));
        }

        private void hot_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.HOT));
        }

        private void zero_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.ZERO_ALCOHOL));
        }

        private void alcohol_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.ALCOHOL));
        }

        private void drink_Click(object sender, EventArgs e)
        {
            SelectButton((Button)sender);
            RefreshMenu(service.GetMenuByType(MenuType.DRINK));
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (menu.SelectedItems[0].Text != null)
                {
                    ManagerForm managerForm = new ManagerForm(service);

                    service.AddEntryToTable(table_name, menu.SelectedItems[0].Text, 1);

                    this.Hide();

                    form.RefreshOrders();
                    form.RefreshTables();

                    form.SelectTable(table_name);
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.DebugLog("Nie wybrałeś nic z menu");
            }
        }
    }
}
