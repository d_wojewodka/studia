using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dominik
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.loginButton = new System.Windows.Forms.Button();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.UsernameLabelMessage = new System.Windows.Forms.Label();
            this.sqliteCommand1 = new Microsoft.Data.Sqlite.SqliteCommand();
            this.VisibilityPicture = new System.Windows.Forms.PictureBox();
            this.PanelHeader = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.FormMinimize = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.FormClose = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.Header = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PasswordLabelMessage = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.VisibilityPicture)).BeginInit();
            this.PanelHeader.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FormMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormClose)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // loginButton
            // 
            this.loginButton.AutoSize = true;
            this.loginButton.BackColor = System.Drawing.Color.RoyalBlue;
            this.loginButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loginButton.FlatAppearance.BorderSize = 0;
            this.loginButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(93)))), ((int)(((byte)(218)))));
            this.loginButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.loginButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginButton.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.loginButton.Location = new System.Drawing.Point(124, 241);
            this.loginButton.Margin = new System.Windows.Forms.Padding(8);
            this.loginButton.Name = "loginButton";
            this.loginButton.Padding = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.loginButton.Size = new System.Drawing.Size(95, 45);
            this.loginButton.TabIndex = 0;
            this.loginButton.Text = "ZALOGUJ";
            this.loginButton.UseVisualStyleBackColor = false;
            this.loginButton.Click += new System.EventHandler(this.authenticateButton_Click);
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.usernameTextBox.Location = new System.Drawing.Point(20, 50);
            this.usernameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.usernameTextBox.MaxLength = 32;
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.PlaceholderText = "Nazwa użytkownika / email";
            this.usernameTextBox.Size = new System.Drawing.Size(280, 27);
            this.usernameTextBox.TabIndex = 1;
            this.usernameTextBox.Text = "dwojewódka";
            this.usernameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.usernameTextBox.Click += new System.EventHandler(this.ClearTextBoxAfterClick);
            this.usernameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.usernameTextBox_TextChanged);
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Candara", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.usernameLabel.Location = new System.Drawing.Point(46, 19);
            this.usernameLabel.Margin = new System.Windows.Forms.Padding(4);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(227, 23);
            this.usernameLabel.TabIndex = 3;
            this.usernameLabel.Text = "Nazwa użytkownika / email";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Candara", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.passwordLabel.Location = new System.Drawing.Point(132, 19);
            this.passwordLabel.Margin = new System.Windows.Forms.Padding(4);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(55, 23);
            this.passwordLabel.TabIndex = 4;
            this.passwordLabel.Text = "Hasło";
            // 
            // UsernameLabelMessage
            // 
            this.UsernameLabelMessage.Font = new System.Drawing.Font("Candara Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.UsernameLabelMessage.ForeColor = System.Drawing.Color.IndianRed;
            this.UsernameLabelMessage.Location = new System.Drawing.Point(19, 81);
            this.UsernameLabelMessage.Name = "UsernameLabelMessage";
            this.UsernameLabelMessage.Size = new System.Drawing.Size(280, 23);
            this.UsernameLabelMessage.TabIndex = 6;
            this.UsernameLabelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sqliteCommand1
            // 
            this.sqliteCommand1.CommandTimeout = 30;
            this.sqliteCommand1.Connection = null;
            this.sqliteCommand1.Transaction = null;
            this.sqliteCommand1.UpdatedRowSource = System.Data.UpdateRowSource.None;
            // 
            // VisibilityPicture
            // 
            this.VisibilityPicture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VisibilityPicture.Image = global::Projekt.Properties.Resources.visibility_dark;
            this.VisibilityPicture.InitialImage = null;
            this.VisibilityPicture.Location = new System.Drawing.Point(253, 0);
            this.VisibilityPicture.Margin = new System.Windows.Forms.Padding(0);
            this.VisibilityPicture.Name = "VisibilityPicture";
            this.VisibilityPicture.Size = new System.Drawing.Size(28, 27);
            this.VisibilityPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.VisibilityPicture.TabIndex = 8;
            this.VisibilityPicture.TabStop = false;
            this.VisibilityPicture.Click += new System.EventHandler(this.VisibilityPicture_Click);
            // 
            // PanelHeader
            // 
            this.PanelHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.PanelHeader.Controls.Add(this.panel6);
            this.PanelHeader.Controls.Add(this.panel7);
            this.PanelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelHeader.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.PanelHeader.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.PanelHeader.Location = new System.Drawing.Point(0, 0);
            this.PanelHeader.Margin = new System.Windows.Forms.Padding(0);
            this.PanelHeader.Name = "PanelHeader";
            this.PanelHeader.Size = new System.Drawing.Size(384, 99);
            this.PanelHeader.TabIndex = 0;
            this.PanelHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelHeader_MouseDown);
            this.PanelHeader.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PanelHeader_MouseMove);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.FormMinimize);
            this.panel6.Controls.Add(this.Title);
            this.panel6.Controls.Add(this.FormClose);
            this.panel6.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.panel6.Size = new System.Drawing.Size(384, 33);
            this.panel6.TabIndex = 1;
            this.panel6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelHeader_MouseDown);
            this.panel6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PanelHeader_MouseMove);
            // 
            // FormMinimize
            // 
            this.FormMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(198)))));
            this.FormMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormMinimize.Image = global::Projekt.Properties.Resources.minimize;
            this.FormMinimize.Location = new System.Drawing.Point(284, 3);
            this.FormMinimize.Name = "FormMinimize";
            this.FormMinimize.Size = new System.Drawing.Size(41, 27);
            this.FormMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FormMinimize.TabIndex = 2;
            this.FormMinimize.TabStop = false;
            this.FormMinimize.Click += new System.EventHandler(this.FormMinimize_Click);
            this.FormMinimize.MouseEnter += new System.EventHandler(this.FormMinimize_MouseEnter);
            this.FormMinimize.MouseLeave += new System.EventHandler(this.FormMinimize_MouseLeave);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Title.Location = new System.Drawing.Point(12, 8);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(110, 17);
            this.Title.TabIndex = 1;
            this.Title.Text = "Panel logowania";
            // 
            // FormClose
            // 
            this.FormClose.BackColor = System.Drawing.Color.IndianRed;
            this.FormClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormClose.Image = global::Projekt.Properties.Resources.close;
            this.FormClose.Location = new System.Drawing.Point(331, 3);
            this.FormClose.Margin = new System.Windows.Forms.Padding(3, 3, 3, 9);
            this.FormClose.Name = "FormClose";
            this.FormClose.Size = new System.Drawing.Size(41, 27);
            this.FormClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FormClose.TabIndex = 0;
            this.FormClose.TabStop = false;
            this.FormClose.Click += new System.EventHandler(this.FormClose_Click);
            this.FormClose.MouseEnter += new System.EventHandler(this.FormClose_MouseEnter);
            this.FormClose.MouseLeave += new System.EventHandler(this.FormClose_MouseLeave);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.Header);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 33);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(384, 66);
            this.panel7.TabIndex = 2;
            // 
            // Header
            // 
            this.Header.AutoSize = true;
            this.Header.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Header.Location = new System.Drawing.Point(112, 17);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(161, 32);
            this.Header.TabIndex = 0;
            this.Header.Text = "ZALOGUJ SIĘ";
            this.Header.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.PasswordLabelMessage);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.passwordLabel);
            this.panel1.Location = new System.Drawing.Point(10, 112);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(15);
            this.panel1.Size = new System.Drawing.Size(318, 121);
            this.panel1.TabIndex = 9;
            // 
            // PasswordLabelMessage
            // 
            this.PasswordLabelMessage.Font = new System.Drawing.Font("Candara Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PasswordLabelMessage.ForeColor = System.Drawing.Color.IndianRed;
            this.PasswordLabelMessage.Location = new System.Drawing.Point(19, 83);
            this.PasswordLabelMessage.Name = "PasswordLabelMessage";
            this.PasswordLabelMessage.Size = new System.Drawing.Size(281, 23);
            this.PasswordLabelMessage.TabIndex = 7;
            this.PasswordLabelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.BackColor = System.Drawing.SystemColors.Window;
            this.panel3.Controls.Add(this.passwordTextBox);
            this.panel3.Controls.Add(this.VisibilityPicture);
            this.panel3.ForeColor = System.Drawing.SystemColors.Window;
            this.panel3.Location = new System.Drawing.Point(19, 46);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(281, 27);
            this.panel3.TabIndex = 5;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.passwordTextBox.Location = new System.Drawing.Point(0, 0);
            this.passwordTextBox.Margin = new System.Windows.Forms.Padding(0, 0, 8, 0);
            this.passwordTextBox.MaxLength = 32;
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PlaceholderText = "Hasło";
            this.passwordTextBox.Size = new System.Drawing.Size(254, 27);
            this.passwordTextBox.TabIndex = 2;
            this.passwordTextBox.Text = "haslo";
            this.passwordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.passwordTextBox.UseSystemPasswordChar = true;
            this.passwordTextBox.Click += new System.EventHandler(this.ClearTextBoxAfterClick);
            this.passwordTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.passwordTextBox_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(64)))), ((int)(((byte)(76)))));
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Location = new System.Drawing.Point(0, 99);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15);
            this.panel2.Size = new System.Drawing.Size(384, 361);
            this.panel2.TabIndex = 10;
            // 
            // panel5
            // 
            this.panel5.AutoSize = true;
            this.panel5.Controls.Add(this.loginButton);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Location = new System.Drawing.Point(23, 31);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(5);
            this.panel5.Size = new System.Drawing.Size(338, 299);
            this.panel5.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.Controls.Add(this.UsernameLabelMessage);
            this.panel4.Controls.Add(this.usernameLabel);
            this.panel4.Controls.Add(this.usernameTextBox);
            this.panel4.Location = new System.Drawing.Point(10, 8);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(15, 15, 15, 0);
            this.panel4.Size = new System.Drawing.Size(319, 104);
            this.panel4.TabIndex = 10;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(64)))), ((int)(((byte)(76)))));
            this.ClientSize = new System.Drawing.Size(384, 460);
            this.Controls.Add(this.PanelHeader);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginForm";
            this.Text = "Panel logowania";
            ((System.ComponentModel.ISupportInitialize)(this.VisibilityPicture)).EndInit();
            this.PanelHeader.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FormMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FormClose)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Button loginButton;
        private TextBox usernameTextBox;
        private Label usernameLabel;
        private Label passwordLabel;
        private Label UsernameLabelMessage;
        private SqliteCommand sqliteCommand1;
        private PictureBox VisibilityPicture;
        private Panel PanelHeader;
        private Label Header;
        private Panel panel1;
        private Panel panel2;
        private Panel panel4;
        private Panel panel5;
        private Panel panel6;
        private Panel panel7;
        private PictureBox FormClose;
        private Label Title;
        private PictureBox FormMinimize;
        private Label PasswordLabelMessage;
        private Panel panel3;
        private TextBox passwordTextBox;
    }
}