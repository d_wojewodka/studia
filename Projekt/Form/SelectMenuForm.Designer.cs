using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dominik
{
	partial class SelectMenuForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.selectButton = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // selectButton
            // 
            this.selectButton.BackColor = System.Drawing.Color.MediumAquamarine;
            this.selectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectButton.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.selectButton.ForeColor = System.Drawing.Color.Green;
            this.selectButton.Location = new System.Drawing.Point(10, 474);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(669, 43);
            this.selectButton.TabIndex = 5;
            this.selectButton.Text = "DODAJ";
            this.selectButton.UseVisualStyleBackColor = false;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // menu
            // 
            this.menu.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader3});
            this.menu.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.menu.Location = new System.Drawing.Point(10, 12);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(394, 456);
            this.menu.TabIndex = 6;
            this.menu.UseCompatibleStateImageBehavior = false;
            this.menu.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nazwa";
            this.columnHeader1.Width = 300;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cena";
            this.columnHeader3.Width = 86;
            // 
            // button1
            // 
            this.button1.AccessibleName = "breakfast";
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(410, 56);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(269, 38);
            this.button1.TabIndex = 7;
            this.button1.Text = "ŚNIADANIA";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.breakfast_Click);
            // 
            // button2
            // 
            this.button2.AccessibleName = "dinner";
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button2.Location = new System.Drawing.Point(410, 100);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(269, 38);
            this.button2.TabIndex = 8;
            this.button2.Text = "OBIADY";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.dinner_Click);
            // 
            // button4
            // 
            this.button4.AccessibleName = "apetizer";
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button4.Location = new System.Drawing.Point(410, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(269, 38);
            this.button4.TabIndex = 9;
            this.button4.Text = "PRZYSTAWKI";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.apetizer_Click);
            // 
            // button3
            // 
            this.button3.AccessibleName = "supper";
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button3.Location = new System.Drawing.Point(410, 144);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(269, 38);
            this.button3.TabIndex = 10;
            this.button3.Text = "KOLACJA";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.supper_Click);
            // 
            // button5
            // 
            this.button5.AccessibleName = "dessert";
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button5.Location = new System.Drawing.Point(410, 188);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(269, 38);
            this.button5.TabIndex = 11;
            this.button5.Text = "DESER";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.dessert_Click);
            // 
            // button6
            // 
            this.button6.AccessibleName = "cold";
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button6.Location = new System.Drawing.Point(410, 298);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(269, 38);
            this.button6.TabIndex = 12;
            this.button6.Text = "NAPOJE ZIMNE";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.cold_Click);
            // 
            // button7
            // 
            this.button7.AccessibleName = "hot";
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button7.Location = new System.Drawing.Point(410, 254);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(269, 38);
            this.button7.TabIndex = 13;
            this.button7.Text = "NAPOJE GORĄCE";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.hot_Click);
            // 
            // button8
            // 
            this.button8.AccessibleName = "zero";
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button8.Location = new System.Drawing.Point(410, 342);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(269, 38);
            this.button8.TabIndex = 14;
            this.button8.Text = "ALKOHOLE 0%";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.zero_Click);
            // 
            // button9
            // 
            this.button9.AccessibleName = "alcohol";
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button9.Location = new System.Drawing.Point(410, 386);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(269, 38);
            this.button9.TabIndex = 15;
            this.button9.Text = "ALKOHOLE";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.alcohol_Click);
            // 
            // button10
            // 
            this.button10.AccessibleName = "drink";
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button10.Location = new System.Drawing.Point(410, 430);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(269, 38);
            this.button10.TabIndex = 16;
            this.button10.Text = "DRINKI";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.drink_Click);
            // 
            // SelectMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 529);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.selectButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SelectMenuForm";
            this.Text = "Panel restauracji";
            this.Load += new System.EventHandler(this.ManagerForm_Load);
            this.ResumeLayout(false);

		}

		#endregion
		private Button selectButton;
		private ListView menu;
		private ColumnHeader columnHeader1;
		private ColumnHeader columnHeader3;
        private Button button1;
        private Button button2;
        private Button button4;
        private Button button3;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button8;
        private Button button9;
        private Button button10;
    }
}