using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projekt;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Dominik {
    public partial class ManagerForm : Form {
        private Service service;

        public ManagerForm(Service service) {
			this.service = service;
            InitializeComponent();
        }

        private void ManagerForm_Load(object sender, EventArgs e) {
            usernameLabel.Text = service!.getActiveUser();
            RefreshTables();
        }

        public void SelectTable(string table_name) {
            foreach (ListViewItem item in tables.Items) {
                if (item.Text == table_name)
                    item.Selected = true;
            }
        }

        public void RefreshTables()
        {
            this.tables.Items.Clear();
            List<TableEntity> tables = service.GetAllTables();

            if (tables.Count == 0)
            {
                ListViewItem item = new ListViewItem("Nie znaleziono żadnego stolika");
                this.tables.Items.Add(item);
            }
            else
            {
                foreach (TableEntity table in tables)
                {
                    ListViewItem item = new ListViewItem(table.getName());
                    item.SubItems.Add(table.getSize().ToString());
                    item.SubItems.Add(table.GetStatusName());
                    item.SubItems.Add(table.getCredentials());
                    item.ForeColor = table.GetStatusColor();

                    this.tables.Items.Add(item);
                }
            }
        }

        public void RefreshOrders() {
            orders.Items.Clear();

            try
            {
                List<OrderEntity> fetch = service.GetOrderByTableName(tables.SelectedItems[0].Text);

                double total = 0.0;

                if (fetch != null)
                {
                    foreach (OrderEntity order in fetch)
                    {
                        ListViewItem item = new ListViewItem(order.getMenu());
                        item.SubItems.Add(order.getQuantity().ToString());
                        item.SubItems.Add((service.GetPrice(order.getMenu()) * order.getQuantity()).ToString("0.00") + "zł");
                        total += service.GetPrice(order.getMenu()) * order.getQuantity(); ;

                        orders.Items.Add(item);
                    }

                    ListViewItem separator = new ListViewItem("");
                    separator.SubItems.Add("");
                    separator.SubItems.Add("________");

                    orders.Items.Add(separator);

                    ListViewItem last = new ListViewItem("");
                    last.SubItems.Add("");
                    last.SubItems.Add(total.ToString("0.00") + "zł");

                    orders.Items.Add(last);
                }
            } catch (ArgumentOutOfRangeException) { }
        }

        private void logout_Click(object sender, EventArgs e) {
            service!.Logout();
			Application.Run(new LoginForm(service));
        }

        protected override void OnFormClosing(FormClosingEventArgs e) {
            Application.Exit();
        }

        private void logoutButton_Click(object sender, EventArgs e) {
            LoginForm loginForm = new LoginForm(service);

            this.Hide();
            loginForm.Show();
        }

        private void button1_Click(object sender, EventArgs e) { }

        private void stoliki_SelectedIndexChanged(object sender, EventArgs e) {
            RefreshOrders();
        }

        private void selectTableButton_Click_1(object sender, EventArgs e) {
            try { 
			    TableEntity table = service.GetTableByName(tables.SelectedItems[0].Text);

                // load order list for table


                string input;
                if (InputDialog.Show("Wpisz dane na jakie zrobić rezerwację", out input) == DialogResult.OK) {
                    service.BookTable(tables.SelectedItems[0].Text, input);
                    RefreshTables();
                }

                Program.DebugLog("Wybrałeś stolik: " + tables.SelectedItems[0].Text);
            } catch (ArgumentOutOfRangeException) {
                Program.DebugLog("Nie wybrałeś żadnego stolika");
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e) {
        }

        private void removeOrderButton_Click(object sender, EventArgs e) {
            try {
                if (tables.SelectedItems[0].Text != "") {
                    service.RemoveFromOrder(tables.SelectedItems[0].Text, orders.SelectedItems[0].Text);
                    RefreshOrders();
                }
            }
            catch (Exception) { }
            
        }

        private void newOrderButton_Click(object sender, EventArgs e) {
            try {
                if (tables.SelectedItems[0].Text != null) {
                    SelectMenuForm selectMenuForm = new SelectMenuForm(service, tables.SelectedItems[0].Text, this);


                    selectMenuForm.Show();
                }
            }
            catch (ArgumentOutOfRangeException) {
                Program.DebugLog("Nie wybrałeś żadnego stolika");
            }
        }

        private void invoiceButton_Click(object sender, EventArgs e) {

        }

        private void cancelBookButton_Click(object sender, EventArgs e) {
            try
            {
                service.CancelBooking(tables.SelectedItems[0].Text);
                RefreshTables();
                RefreshOrders();
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.DebugLog("Nie wybrałeś żadnego stolika");
            }
        }
    }
}
