﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public class InputDialog : Form
    {
        private Label label;
        private TextBox inputTextBox;
        private Button okButton;
        private Button cancelButton;

        public InputDialog()
        {
            // Initialize the form
            this.Text = "Input Dialog";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Width = 300;
            this.Height = 150;

            // Initialize the text box
            label = new Label();
            label.Location = new Point(10, 10);
            label.Width = this.Width - 40;
            this.Controls.Add(label);

            // Initialize the text box
            inputTextBox = new TextBox();
            inputTextBox.Location = new Point(10, label.Bottom + 10);
            inputTextBox.Width = this.Width - 40;
            this.Controls.Add(inputTextBox);

            // Initialize the OK button
            okButton = new Button();
            okButton.Text = "OK";
            okButton.Location = new Point(10, inputTextBox.Bottom + 10);
            okButton.Click += new EventHandler(okButton_Click!);
            this.Controls.Add(okButton);

            // Initialize the Cancel button
            cancelButton = new Button();
            cancelButton.Text = "Cancel";
            cancelButton.Location = new Point(okButton.Right, okButton.Top);
            cancelButton.Click += new EventHandler(cancelButton_Click!);
            this.Controls.Add(cancelButton);
        }

        public void SetTask(string task)
        {
            label.Text = task;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public string InputValue
        {
            get { return inputTextBox.Text; }
        }

        public static DialogResult Show(string task, out string input)
        {
            InputDialog dialog = new InputDialog();
            dialog.SetTask(task);
            dialog.Text = "";
            DialogResult result = dialog.ShowDialog();
            input = dialog.InputValue;
            return result;
        }
    }
}
