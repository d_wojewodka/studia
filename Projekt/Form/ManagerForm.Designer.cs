using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dominik
{
	partial class ManagerForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.logoutButton = new System.Windows.Forms.Button();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.bookButton = new System.Windows.Forms.Button();
            this.orders = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.newOrderButton = new System.Windows.Forms.Button();
            this.invoiceButton = new System.Windows.Forms.Button();
            this.removeOrderButton = new System.Windows.Forms.Button();
            this.cancelBookButton = new System.Windows.Forms.Button();
            this.name = new System.Windows.Forms.ColumnHeader();
            this.size = new System.Windows.Forms.ColumnHeader();
            this.available = new System.Windows.Forms.ColumnHeader();
            this.credentials = new System.Windows.Forms.ColumnHeader();
            this.tables = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // logoutButton
            // 
            this.logoutButton.AutoSize = true;
            this.logoutButton.BackColor = System.Drawing.Color.LightCoral;
            this.logoutButton.BackgroundImage = global::Projekt.Properties.Resources.minimize_dark;
            this.logoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.logoutButton.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.logoutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logoutButton.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.logoutButton.Location = new System.Drawing.Point(963, 12);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(140, 31);
            this.logoutButton.TabIndex = 1;
            this.logoutButton.TabStop = false;
            this.logoutButton.Text = "WYLOGUJ";
            this.logoutButton.UseVisualStyleBackColor = false;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // usernameLabel
            // 
            this.usernameLabel.Font = new System.Drawing.Font("Candara", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.usernameLabel.Location = new System.Drawing.Point(719, 14);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(238, 23);
            this.usernameLabel.TabIndex = 2;
            this.usernameLabel.Text = "name lastName";
            this.usernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bookButton
            // 
            this.bookButton.BackColor = System.Drawing.Color.MediumAquamarine;
            this.bookButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bookButton.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.bookButton.ForeColor = System.Drawing.Color.Green;
            this.bookButton.Location = new System.Drawing.Point(10, 429);
            this.bookButton.Name = "bookButton";
            this.bookButton.Size = new System.Drawing.Size(245, 43);
            this.bookButton.TabIndex = 5;
            this.bookButton.Text = "REZERWUJ STOLIK";
            this.bookButton.UseVisualStyleBackColor = false;
            this.bookButton.Click += new System.EventHandler(this.selectTableButton_Click_1);
            // 
            // orders
            // 
            this.orders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.orders.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.orders.Location = new System.Drawing.Point(522, 64);
            this.orders.Name = "orders";
            this.orders.Size = new System.Drawing.Size(394, 408);
            this.orders.TabIndex = 6;
            this.orders.UseCompatibleStateImageBehavior = false;
            this.orders.View = System.Windows.Forms.View.Details;
            this.orders.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nazwa";
            this.columnHeader1.Width = 250;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Ilość";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cena";
            this.columnHeader3.Width = 80;
            // 
            // newOrderButton
            // 
            this.newOrderButton.BackColor = System.Drawing.Color.MediumAquamarine;
            this.newOrderButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newOrderButton.Font = new System.Drawing.Font("Candara", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.newOrderButton.ForeColor = System.Drawing.Color.Green;
            this.newOrderButton.Location = new System.Drawing.Point(933, 64);
            this.newOrderButton.Name = "newOrderButton";
            this.newOrderButton.Size = new System.Drawing.Size(167, 43);
            this.newOrderButton.TabIndex = 7;
            this.newOrderButton.Text = "NOWE ZAMÓWIENIE";
            this.newOrderButton.UseVisualStyleBackColor = false;
            this.newOrderButton.Click += new System.EventHandler(this.newOrderButton_Click);
            // 
            // invoiceButton
            // 
            this.invoiceButton.Location = new System.Drawing.Point(933, 429);
            this.invoiceButton.Name = "invoiceButton";
            this.invoiceButton.Size = new System.Drawing.Size(167, 41);
            this.invoiceButton.TabIndex = 8;
            this.invoiceButton.Text = "Rachunek";
            this.invoiceButton.UseVisualStyleBackColor = true;
            this.invoiceButton.Visible = false;
            this.invoiceButton.Click += new System.EventHandler(this.invoiceButton_Click);
            // 
            // removeOrderButton
            // 
            this.removeOrderButton.BackColor = System.Drawing.Color.MistyRose;
            this.removeOrderButton.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.removeOrderButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeOrderButton.Font = new System.Drawing.Font("Candara", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.removeOrderButton.ForeColor = System.Drawing.Color.IndianRed;
            this.removeOrderButton.Location = new System.Drawing.Point(933, 122);
            this.removeOrderButton.Name = "removeOrderButton";
            this.removeOrderButton.Size = new System.Drawing.Size(167, 40);
            this.removeOrderButton.TabIndex = 9;
            this.removeOrderButton.Text = "USUN ZAMÓWIENIE";
            this.removeOrderButton.UseVisualStyleBackColor = false;
            this.removeOrderButton.Click += new System.EventHandler(this.removeOrderButton_Click);
            // 
            // cancelBookButton
            // 
            this.cancelBookButton.BackColor = System.Drawing.Color.MistyRose;
            this.cancelBookButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelBookButton.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.cancelBookButton.ForeColor = System.Drawing.Color.IndianRed;
            this.cancelBookButton.Location = new System.Drawing.Point(261, 429);
            this.cancelBookButton.Name = "cancelBookButton";
            this.cancelBookButton.Size = new System.Drawing.Size(245, 43);
            this.cancelBookButton.TabIndex = 10;
            this.cancelBookButton.Text = "USUŃ REZERWACJĘ";
            this.cancelBookButton.UseVisualStyleBackColor = false;
            this.cancelBookButton.Click += new System.EventHandler(this.cancelBookButton_Click);
            // 
            // name
            // 
            this.name.Text = "Nazwa";
            this.name.Width = 100;
            // 
            // size
            // 
            this.size.Text = "Rozmiar";
            this.size.Width = 70;
            // 
            // available
            // 
            this.available.Text = "Dostępność";
            this.available.Width = 120;
            // 
            // credentials
            // 
            this.credentials.Text = "Personalia";
            this.credentials.Width = 200;
            // 
            // tables
            // 
            this.tables.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.size,
            this.available,
            this.credentials});
            this.tables.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tables.Location = new System.Drawing.Point(12, 16);
            this.tables.Name = "tables";
            this.tables.Size = new System.Drawing.Size(494, 398);
            this.tables.TabIndex = 4;
            this.tables.UseCompatibleStateImageBehavior = false;
            this.tables.View = System.Windows.Forms.View.Details;
            this.tables.SelectedIndexChanged += new System.EventHandler(this.stoliki_SelectedIndexChanged);
            // 
            // ManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(1112, 484);
            this.Controls.Add(this.cancelBookButton);
            this.Controls.Add(this.removeOrderButton);
            this.Controls.Add(this.invoiceButton);
            this.Controls.Add(this.newOrderButton);
            this.Controls.Add(this.orders);
            this.Controls.Add(this.bookButton);
            this.Controls.Add(this.tables);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.logoutButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ManagerForm";
            this.Text = "Panel restauracji";
            this.Load += new System.EventHandler(this.ManagerForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private Button logoutButton;
		private Label usernameLabel;
		private Button bookButton;
		private ListView orders;
		private ColumnHeader columnHeader1;
		private ColumnHeader columnHeader2;
		private ColumnHeader columnHeader3;
		private Button newOrderButton;
		private Button invoiceButton;
		private Button removeOrderButton;
        private Button cancelBookButton;
        private ColumnHeader name;
        private ColumnHeader size;
        private ColumnHeader available;
        private ColumnHeader credentials;
        private ListView tables;
    }
}